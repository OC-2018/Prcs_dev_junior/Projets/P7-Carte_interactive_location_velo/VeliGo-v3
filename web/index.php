<?php

require_once '../vendor/autoload.php';


// DEBUG CODE //
    //echo '<pre>';
        //echo print_r(var_dump(__DIR__ ));
    //echo '</pre>';


// -------------------- Config Twig ---------------------- //

$loader = new Twig_Loader_Filesystem(__DIR__ . '/../templates');

$twig = new Twig_Environment($loader, array(
    'cache' => false, // __DIR__ . '/tmp/cache'
));



// -------------------- Routing ---------------------- //

$page = 'home';

if(isset($_GET['p']))
{
    $page = $_GET['p'];
}



if($page === 'home')
{
    echo $twig->render('home.html.twig');
}
elseif ($page === "contact")
{
    echo $twig->render('contact.html.twig');
}
elseif ($page === "apropos")
{
    echo $twig->render('a-propos.html.twig');
}