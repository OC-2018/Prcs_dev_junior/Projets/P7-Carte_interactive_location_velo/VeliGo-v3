$(document).ready(function() {


    $("#booking-form").submit(function(e)
    {
        e.preventDefault();   // Not refresh page

        var stationName = $('#station-name').text();
        var firstname = $('#nom').val();
        var lastname = $('#prenom').val();
        var canvas = document.getElementById("canvas-sign");


        // DEBUG //
        // alert("Station : " + stationName + "\n Prénom : " + firstname + "\n Nom : " + lastname + "\n Data CanvasURL : \n" + canvas.toDataURL('image/png', 1.0));


        // Check if a reservation is in progress by checking if a countdown is running
        if(Timer.x !== null)
        {
            /* DEBUG */ console.log("Stop the timer for the precedent reservation || And delete data from SessionStorage");
            Timer.stopTimer(Timer.x);
            /* DEBUG */ console.log("Restart new timer");
            Timer.init();

            // Delete all data from sessionStorage
            sessionStorage.clear();

            // Set data to sessionStorage
            sessionStorage.setItem('user_firstname', firstname);
            sessionStorage.setItem('user_lastname', lastname);
            sessionStorage.setItem('station_name', stationName);
            sessionStorage.setItem('signature', canvas.toDataURL('image/png', 1.0));

        }
        else {
            /* DEBUG */ console.log("Start timer for the first reservation");
            Timer.init(); // Start the timer for 20 minutes

            // Set data to sessionStorage
            sessionStorage.setItem('user_firstname', firstname);
            sessionStorage.setItem('user_lastname', lastname);
            sessionStorage.setItem('station_name', stationName);
            sessionStorage.setItem('signature', canvas.toDataURL('image/png', 1.0));

        }

        Panel.close();

    });

});