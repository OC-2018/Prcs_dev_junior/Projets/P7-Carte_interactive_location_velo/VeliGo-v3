var Panel = {

    bookingP: $('#booking_panel'),
    mapDiv: $('#map'),
    btnClose: $('#btn_close_booking_panel'),
    btnFormAnnuler: $('#btn_form_annuler'),
    btnFormValider: $('#valider'),

    open: function() {
        this.bookingP.css("display", "flex");
        this.bookingP.fadeIn("slow", function() {});
        this.mapDiv.removeClass('col-lg-12');
        this.mapDiv.addClass('col-lg-9');
        this.mapDiv.css("right", "12.5%");
    },

    close: function() {
        this.bookingP.css("display", "none");
        this.mapDiv.removeClass('col-lg-9');
        this.mapDiv.addClass('col-lg-12');
        this.mapDiv.css("right", "0");
        Canvas.reset();
        $('#nom').val("");
        $('#prenom').val("");
    },

};
