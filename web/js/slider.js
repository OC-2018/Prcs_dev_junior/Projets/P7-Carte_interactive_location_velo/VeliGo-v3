var Slider = {

    btnOpen: $('#link_guide'),
    btnClose: $('#btn_close_slider'),
    slideshow: $('#slideshow'),
    slideIndex: 0,

    /**
     * Display the first slider
     */
    init: function () {
        Slider.showSlide(1);
    },

/**
     * Slider administrator
     *
     * @param n
     */
    sliderChoice: function(n) {
        var i;
        var x           = document.getElementsByClassName("js-mySlides");
        var dots        = document.getElementsByClassName("marker");
        var arianeElts  = document.getElementsByClassName("ariane");

        if (n > x.length) { Slider.slideIndex = 1        }
        if (n < 1)        { Slider.slideIndex = x.length }

        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }

        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" slider_btn_active", "")
        }

        for (i = 0; i < arianeElts.length; i++) {
            arianeElts[i].className = arianeElts[i].className.replace(" ariane_active", "")
        }

        x[Slider.slideIndex-1].style.display        =  "flex";
        dots[Slider.slideIndex-1].className         += " slider_btn_active";
        arianeElts[Slider.slideIndex-1].className   += " ariane_active";
    },

    /**
     * Displaying the previous slide
     */
    previousSlide: function() {
        Slider.sliderChoice(Slider.slideIndex -= 1);
    },

    /**
     * Displaying the n(param) slide
     *
     * @param index
     */
    showSlide: function (index) {
        Slider.sliderChoice(Slider.slideIndex = index);
    },

    /**
     * Displaying the next slide
     */
    nextSlide: function() {
        Slider.sliderChoice(Slider.slideIndex += 1);
    },

    /**
     * Open the slideshow
     */
    open: function()    {
        Slider.slideshow.fadeIn();
    },

    /**
     * Close the slideshow
     */
    close: function()   {
        Slider.slideshow.fadeOut();
        // Remove the css style for active link
        $( '#navRight' ).find( 'li.nav_item.active_nav_item' ).removeClass( 'active_nav_item' );
        // Add active link class for the current page (Map page)
        $( '#navRight' ).find( 'li#nav_item_map' ).addClass( 'active_nav_item' );
    }

};






