var M = {

    lyon: {lat: 45.764043, lng: 4.835659},
    api: 'https://api.jcdecaux.com/vls/v1/stations?apiKey=00ea9bf068cf830f6e53d698c417562ad4b03dff&contract=Lyon',

    // Initialization
    init: function () {
        map = new google.maps.Map(document.getElementById('map'), {
            center: M.lyon,
            zoom: 14,
            minZoom: 11,
            disableDefaultUI: true,
            zoomControl: true,
            keyboardShortcuts: true,
            gestureHandling: "cooperative",
            styles: styleMap
        });

        M.createControlCenter();
        M.callApi();

    },

    /**
     * Définition des propryiété CSS du bouton de centrage de la carte
     */
    centerControl: function (controlDiv, map) {
        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.position = 'relative';
        controlUI.style.bottom = '30px';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '3px';
        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '22px';

        controlUI.style.textAlign = 'center';
        controlUI.title = 'Cliquez pour centrer la carte sur Lyon';
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(25,25,25)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '16px';
        controlText.style.lineHeight = '38px';
        controlText.innerHTML = 'Centrer la carte';
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Lyon.
        controlUI.addEventListener('click', function () {
            map.setCenter(M.lyon);
        });
    },

    /*
     * Insertion et positionnement du bouton CenterControl dans la carte
     */
    createControlCenter: function () {
        var centerControlDiv = document.createElement('div');
        var centerControl = new M.centerControl(centerControlDiv, map);
        centerControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(centerControlDiv);
    },

    /*
     * Initialise la bibliothèque MarkerClusterer, qui va permettre le regroupement
     *  de marqueur selon le niveau de zoom de la carte
     */
    initMarkerGrouping: function (map, markers) {
        new MarkerClusterer(map, markers, {imagePath: 'img/m/m'});
    },

    /*
     * Récupération des données JSON de l'API JCDecaux sur la ville de Lyon,
     * afin de placer les marqueurs sur chacunes des   localisations des stations
     */
    callApi: function ()
    {
        Ajax.requestAjax(M.api, "GET", function (response)
        {
            var stations = JSON.parse(response);
            // var personalIcon = new google.maps.MarkerImage('img/icone_map/ico-1.png');

            // création d'un tableau qui contiendra toutes les localisations des stations de Lyon
            var markers = [];

            // Boucle permettant de traiter chacun des marqueurs individuellement
            stations.forEach(function (station) {

                var contentString =
                    '<div id="content" style="text-align: center;">' +
                    '<h4 style="font-weight: bold;">Station n° ' + station.name + '</h4>' +
                    '<hr>' +
                    '<p style="font-weight: bold;">Adresse : </p>' +
                    '<p style="font-style: italic;">' + station.address + '</p>' +
                    '<hr>' +
                    '<p><span style="font-weight: bold;">Status de la station : </span>' + station.status + '</p>' +
                    '<hr>' +
                    '<div>' +
                    '<p><span style="font-weight: bold;">Vélo disponible : </span>' + station.available_bikes + '</p>' +
                    '<p><span style="font-weight: bold;">Emplacement disponible : </span>' + station.available_bike_stands + '</p>' +
                    '</div>' +
                    '<hr>' +
                    '</div>';

                // Définit ContentString comme contenu  pour InfoWindow, ainsi que sa largaur Max
                var infowindow = new google.maps.InfoWindow({
                    content: contentString,
                    maxWidth: 250
                });

                // Création de chaques marqueurs et insertion dans la Map
                var marker = new google.maps.Marker({
                    position: {lat: station.position.lat, lng: station.position.lng},
                    map: map,
                    animation: google.maps.Animation.DROP,
                    // icon : monIconPerso,
                });

                // Ajout d'un ecouteur sur chaque marqueur
                marker.addListener("click", function () {

                    // Affichage des infos station lors du clic sur un marqueur
                    if (infowindow) {
                        infowindow.setMap(null);        // Fermeture de la fenetre InfoWindow active
                        infowindow.open(map, marker);   // Ouverture de la fenetre InfoWindow
                    } else if (!infowindow) {
                        infowindow.open(map, marker);
                    }

                    /*
                     * Initialise l'animation Google Map Bounce()
                     **/
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                    /*
                     *Stop l'anmation Bounce() du marker après un délais de 1.5 secondes
                     * */
                    setTimeout(function() {
                        marker.setAnimation(null);
                    }, 1500);

                    // On verifie si le bouton n'existe pas déjà
                    if (!document.getElementById('btnBookingInfoWindow')) {
                        // Création d'un bouton réserver à la volée
                        var btn_booking_panel = document.createElement('button');
                        btn_booking_panel.id = "btnBookingInfoWindow";
                        btn_booking_panel.textContent = "Effectuer une réservation";
                        var content = document.getElementById('content');
                        content.appendChild(btn_booking_panel);
                    }

                    // Ajout d'un ecouteur sur l'event Click
                    btnBookingInfoWindow.addEventListener("click", function () {

                        infowindow.setMap(null); // Fermeture de la fenetre InfoWindow a

                        // ouverture du panneau de reservation
                        var divMap = document.querySelector('#map');
                        var bookingP = document.querySelector('#booking_panel');
                        bookingP.style.display =  "flex";
                        divMap.classList.remove('col-lg-12');
                        divMap.classList.add('col-lg-9');
                        divMap.style.right = "12.5%";

                        // Intègration des donnees
                        var stationName = document.querySelector('#station-name');
                        stationName.textContent = station.name;

                        var stationAddress = document.querySelector('#station-address');
                        stationAddress.textContent = station.address;

                        var stationStatus = document.querySelector('#station-status');
                        stationStatus.innerHTML = ""; // On vide d'abord le ccontenu précédent

                        // Traduction de la valeur récupérer en français
                        if (station.status === 'OPEN') {
                            stationStatus.innerHTML += '<span style="text-transform:uppercase; color: green !important; font-weight: bold; font-size: 1em; text-align: center;">ouvert</span>';
                        } else if (station.status === "CLOSE") {
                            stationStatus.innerHTML += '<span style="text-transform: uppercase; color: red; font-weight: bold; font-size: 1em; text-align: center;" >fermer</span>';
                        } else {
                            stationStatus.innerHTML += '<span style="text-transform: uppercase; color: orange; font-weight: bold; font-size: 1em; text-align: center;" >Non définit</span>';
                        }

                        var available_bikes = document.querySelector('#station-available-bikes');
                        available_bikes.textContent = station.available_bikes;

                        var station_available_bike_stands = document.querySelector('#station-available-bike-stands');
                        station_available_bike_stands.textContent = station.available_bike_stands;
                    });


                });

                // Insertion de chaque marqueur dans le tableau intitulé "markers", afin de pouvoir utiliser le Regroupement de marqueur de Google Map
                markers.push(marker);
            });


            // Instanciation de l'objet MarkerClusterer qui vas initialiser le regroupement de marqueur de Ms
            // new MarkerClusterer(map, markers,{imagePath: 'img/m/m'});
            M.initMarkerGrouping(map, markers);
        })
    }
}
