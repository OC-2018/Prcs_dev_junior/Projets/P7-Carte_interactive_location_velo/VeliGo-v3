var Canvas = {

    mouseDown: 0,
    touchX: 0,
    touchY: 0,
    lastX: -1,
    lastY: -1,

    init: function () {
        ctx = document.getElementById('canvas-sign').getContext('2d');
        document.getElementById('valider').style.display = "none";

        var canvas = document.getElementById('canvas-sign');
        canvas.addEventListener("mousedown", function (e) {
            Canvas.desktop_mouseDown(e)
        });
        canvas.addEventListener("mouseup", function (e){
            Canvas.desktop_mouseUp(e)
        });
        canvas.addEventListener("mousemove", function (e){
            Canvas.desktop_mouseMove(e)
        });
        canvas.addEventListener("touchstart", function (e){
            Canvas.mobile_touchStart(e)
        });
        canvas.addEventListener("touchend", function (e){
            Canvas.mobile_touchEnd(e)
        });
        canvas.addEventListener("touchmove", function (e){
            Canvas.mobile_touchMove(e)
        });



    },

    reset: function () {
        ctx.clearRect(0, 0, document.getElementById('canvas-sign').width, document.getElementById('canvas-sign').height);
        document.getElementById('valider').style.display = "none";
    },

    drawLine: function (ctx, x, y, size) {
        //Si lastX n'est pas définie, définir lastX et lastY a la position courante
        if (Canvas.lastX == -1) {
            Canvas.lastX = x;
            Canvas.lastY = y;
        }

        // déplace a la précedente position
        ctx.strokeStyle = "#45505b";
        ctx.lineCap = "round";

        ctx.beginPath();

        ctx.moveTo(Canvas.lastX, Canvas.lastY);

        ctx.lineTo(x, y);

        ctx.lineWidth = size;
        ctx.stroke();

        ctx.closePath();

        //Met la dérniére position comme référence a la position courante
        Canvas.lastX = x;
        Canvas.lastY = y;

        document.getElementById('valider').style.display = "block";
    },

    desktop_mouseDown: function () {
        Canvas.mouseDown = 1;
        Canvas.drawLine(ctx, Canvas.mouseX, Canvas.mouseY, 4);
    },

    desktop_mouseUp: function () {
        Canvas.mouseDown = 0;
        Canvas.lastX = -1;
        Canvas.lastY = -1;
    },

    desktop_mouseMove: function (e) {
        if (Canvas.mouseDown === 1) {
            Canvas.drawLine(ctx, e.offsetX * 1, e.offsetY * 1, 2);
        }
    },

    mobile_touchStart: function (e) {
        Canvas.drawLine(ctx, e.offsetX * 1, e.offsetY * 1, 2);
        event.preventDefault();
    },

    mobile_touchEnd: function () {
        Canvas.lastX = -1;
        Canvas.lastY = -1;
    },

    mobile_touchMove: function (e) {
        event.preventDefault();
        e.offsetX = e.touches[0].pageX - e.touches[0].target.offsetLeft;
        e.offsetY = e.touches[0].pageY - e.touches[0].target.offsetTop;
        Canvas.drawLine(ctx, e.offsetX * 1, e.offsetY * 1, 2);
    },

};



