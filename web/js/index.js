$(document).ready(function(){

    // ************ Object initialization ************* //
    M.init();
    Canvas.init();
    Slider.init();


    //**************** Variables **********************//

    // *************** Buttons handler *************** //

    // BOOKING PANEL //
    Panel.btnClose.on( "click" , function()  {
        Panel.close();
    });

    Panel.btnFormAnnuler.on( "click" , function()  {
        Panel.close();
    });

    // SLIDESHOW //
    Slider.btnClose.on("click", function() {
        Slider.close();
    });
    Slider.btnOpen.on("click", function() {
        Slider.open();
    });


    $( '#myNavbar a' ).on( 'click', function () {
        $( '#navRight' ).find( 'li.nav_item.active_nav_item' ).removeClass( 'active_nav_item' );
        $( this ).parent( 'li' ).addClass( 'active_nav_item' );
    });



});
