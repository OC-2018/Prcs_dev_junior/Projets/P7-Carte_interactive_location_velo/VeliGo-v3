

var Timer = {

    prefix: $('#prefixe'),
    bookingP: $('#booking_panel'),
    timerBlocElem: $('.timer_bloc_elem'),
    blocReservationStatu: $('#reservation_status'),
    btnAnnulerReservation: $('#btn_stop_timer'),
    x: null,

    init: function (targetDate) {

        // Display on the timer
        Timer.display();
        Timer.displayReservation();

        var time;

        if(targetDate) { // If refresh page, get the time value in sessionStorage
            time = new Date().getTime() + Number(targetDate);
        } else {
            time = new Date().getTime() + 1200000; // Set the date we're counting down to (1200000 = 20 minutes)
        }

        Timer.x = setInterval(function () { // Update the count down every 1 second

            var now = new Date().getTime(); // Get todays date and time
            var target = time - now;        // Find the distance between now an the count down date

            Timer.setTimeInSessionStorage(target);

            // Time calculations for days, hours, minutes and seconds
            var minutes = Math.floor((target % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((target  % (1000 * 60)) / 1000);

            if (minutes !== -1 && seconds !== -1) {
                var blocMin = document.getElementById("timer_min");
                var blocSec = document.getElementById("timer_sec");

                // Output the result in an element with id="demo"
                blocMin.innerHTML = minutes;
                blocSec.innerHTML = seconds;
            }
            else {// If the count down is over, write some text
                Timer.stopTimer();
            }

        }, 1000);

    },

    /**
     * Save the countdown in sessionStorage when refresh page
     *
     * @param time
     */
    setTimeInSessionStorage: function (time) {
        sessionStorage.setItem('timer-target', time);
    },

    /**
     * Display the text reservation on the footer
     */
    displayReservation: function () {
        var  stationName;
        if(sessionStorage.getItem('station_name')) {
            stationName = sessionStorage.getItem('station_name');
        } else {
            stationName = $('#station-name').text();
        }

        document.getElementById("prefixe").innerHTML = "1 vélo réservé à la station " + stationName ;
        Timer.blocReservationStatu.css('background-color', 'rgb(97, 204, 101)');
        Timer.prefix.css('color', 'rgb(29, 8, 185)');
    },

    /**
     * Display and start the timer
     */
    display: function () {
        Timer.blocReservationStatu.css('align-self', 'flex-start');
        Timer.blocReservationStatu.css('display', 'block');
        Timer.timerBlocElem.css("display", "flex");
        Timer.btnAnnulerReservation.css("display", "block");
    },

    /**
     * Display off the HTML timer bloc
     */
    clearDisplayTimer: function () {
        Timer.blocReservationStatu.css('align-self', 'center');
        Timer.timerBlocElem.css("display", "none");
        Timer.btnAnnulerReservation.css("display", "none");
        document.getElementById('prefixe').innerHTML = "Aucune réservation en cours.";
        Timer.blocReservationStatu.css('background-color', 'rgb(204, 238, 255)');
        Timer.prefix.css('color', 'rgb(135, 25, 63)');
        Timer.blocReservationStatu.css('display', 'none');
    },

    /**
     * Stop the timer and display off the HTML
     */
    stopTimer: function () {
        clearInterval(Timer.x);
        Timer.clearDisplayTimer();
        Timer.x = null;
        // Delete all data from sessionStorage
        sessionStorage.clear();
    }

};




$(document).ready(function() {

    // *************** Timer Restart if page refresh ************ //
    var saveTime;

    if(saveTime = sessionStorage.getItem('timer-target')) {
        Timer.init(saveTime);
    }
});

