# Projet 7 - Parcours Développeur web junior

##### Installation des dépendances de l'application

 - ` composer install`

##### Hôte virtuel 
 Création d'un hôte virtuel qui pointe vers le dossier **/web** du projet
 
 *Avec Apache Wamp :*
    
    *  Clic gauche sur l'icône Wamp dans la barre d'outil windowd
    *  Sélectionnez l'onglet "Vos VirtualHosts"
    *  Cliquez tous en-bas sur l'onglet intitulé "Gestion Virtualhost"
    *  Entrez le nom de votre hôte virtuel dans le premier champ
    *  Entrez le chemin absolu vers le dossier /web de l'application
    *  Et terminer par Valider
    
  ### OU Lancer un serveur web php
  
    *  Ouvrez un terminal à la racine de l'application
    *  Et entrez la commande suivante:
    
   `php -S localhost:4242 -d display_errors=1 -t web`
 
 